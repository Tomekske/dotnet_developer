﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// </summary>
    public class StringTests : ITest
    {
        public void Run()
        {
            // TODO:
            // Complete the methods below

            AnagramTest();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            var word = "stop";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };
                
            foreach (var possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
            }
        }

        private void GetUniqueCharsAndCount()
        {
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";
            bool u = false;
            int counter = 0, lc = 0;
            List<string> uniqueChars = new List<string>();

            // TODO:
            // Write an algoritm that gets the unique characters of the word below 
            // and counts the number of occurences for each character found
            for(int i = 0; i < word.Length;i++)
            {
                u = false;
                for(int j = 0; j < i; j++)
                {
                    if(word[j] == word[i])
                    {
                        u = true;
                    }
                }

                if(!u)
                {
                    counter++;
                    uniqueChars.Add(word[i].ToString());
                }
            }

            Console.WriteLine("Number of unique chars: {0}", counter);

            foreach (string i in uniqueChars)
            {
                lc++;
                Console.WriteLine("{0}. {1}",lc,i);
            }
        }
    }

    public static class StringExtensions
    {
        public static bool IsAnagram(this string a, string b)
        {
            // TODO: 
            // Write logic to determine whether a is an anagram of b
            string sort_a = sortString(a);
            string sort_b = sortString(b);

            if (sort_a == sort_b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static string sortString(string str)
        {
            char[] charsArray = str.ToCharArray();
            Array.Sort(charsArray);
            return new string(charsArray);
        }
    }
}
