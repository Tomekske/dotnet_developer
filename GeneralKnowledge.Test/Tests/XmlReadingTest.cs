﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the 
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test";  } }

        public void Run()
        {
            var xmlData = Resources.SamplePoints;
            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z


            PrintOverview(xmlData);
        }

        private void PrintOverview(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            List<double> list_temp = new List<double>();
            List<double> list_pH = new List<double>();
            List<double> list_phosphate = new List<double>();
            List<double> list_chloride = new List<double>();
            List<double> list_nitrate = new List<double>();
            double[,] data = new double[5, 3];
            

            foreach (XmlNode node in doc.DocumentElement)
            {
                foreach (XmlNode child in node.ChildNodes)
                {
                    String tag = child.OuterXml;
                    if (tag.Contains("temperature"))
                    {
                        list_temp.Add(double.Parse(child.InnerText , System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)); //Comma
                        list_temp.Sort();

                        data[0, 0] = list_temp[0];
                        data[0, 1] = list_temp.Average();
                        data[0, 2] = list_temp[list_temp.Count - 1];

                    }
                    else if (tag.Contains("pH"))
                    {
                        list_pH.Add(double.Parse(child.InnerText, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)); //Comma
                        list_pH.Sort();

                        data[1, 0] = list_pH[0];
                        data[1, 1] = list_pH.Average();
                        data[1, 2] = list_pH[list_pH.Count - 1];
                    }
                    else if (tag.Contains("Phosphate"))
                    {
                        list_phosphate.Add(double.Parse(child.InnerText, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)); //Comma
                        list_phosphate.Sort();

                        data[2, 0] = list_phosphate[0];
                        data[2, 1] = list_phosphate.Average();
                        data[2, 2] = list_phosphate[list_phosphate.Count - 1];

                    }
                    else if (tag.Contains("Chloride"))
                    {
                        list_chloride.Add(double.Parse(child.InnerText, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)); //Comma
                        list_chloride.Sort();

                        data[3, 0] = list_chloride[0];
                        data[3, 1] = list_chloride.Average();
                        data[3, 2] = list_chloride[list_chloride.Count - 1];

                    }
                    else if (tag.Contains("Nitrate"))
                    {
                        list_nitrate.Add(double.Parse(child.InnerText, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)); //Comma
                        list_nitrate.Sort();

                        data[4, 0] = list_nitrate[0];
                        data[4, 1] = list_nitrate.Average();
                        data[4, 2] = list_nitrate[list_nitrate.Count - 1];
                    }
                }
            }

            Console.WriteLine("Parameters\tmin\taverage\tmax");
            Console.WriteLine("Temperature\t{0:0.00}\t{1:0.00}\t{2:0.00}", data[0,0],data[0,1],data[0,2]);
            Console.WriteLine("pH\t\t{0:0.00}\t{1:0.00}\t{2:0.00}", data[1, 0], data[1, 1], data[1, 2]);
            Console.WriteLine("Chloride\t{0:0.00}\t{1:0.00}\t{2:0.00}", data[2, 0], data[2, 1], data[2, 2]);
            Console.WriteLine("Phosphate\t{0:0.00}\t{1:0.00}\t{2:0.00}", data[3, 0], data[3, 1], data[3, 2]);
            Console.WriteLine("Nitrate\t\t{0:0.00}\t{1:0.00}\t{2:0.00}", data[4, 0], data[4, 1], data[4, 2]);
        }
    }
}
