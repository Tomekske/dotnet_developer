﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        class CSV
        {
            public string Asset { get; set; } //nodig
            public string Mime { get; set; } //nodig
            public string Country { get; set; } //nodig
        }

        public void Run()
        {
            // TODO: 
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            var csvFile = Resources.AssetImport;
            string path = @"..\\..\\Resources\\New_CSV.csv";
            StreamWriter sw = File.CreateText(path);
            int counter = 0;
            List<CSV> csv_list = new List<CSV>();
            string[] csvText = csvFile.Split('\n');
            var regex = new Regex("(?<=^|,)(\"(?:[^\"]|\"\")*\"|[^,]*)");



            foreach (string line in csvText.Skip(1))
            {
                List<string> rm_list = new List<string>();
                counter = 0;

                foreach (Match m in regex.Matches(line))
                {
                    counter++;
                    rm_list.Add(m.Value);
                }

                if (counter == 7)
                {
                    CSV csv = new CSV { Asset = rm_list[0], Mime = rm_list[2], Country = rm_list[5] };
                    sw.WriteLine("{0},{1},{2}", csv.Asset, csv.Mime, csv.Country);
                }
            }
            Console.WriteLine("Domain model saved to: Resources/New_CSV.csv");

        }
    }
}
