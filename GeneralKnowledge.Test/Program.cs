﻿using GeneralKnowledge.Test.App.Tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App
{
    class Program
    {
        static void Main(string[] args)
        {
           // String manipulations
           Console.WriteLine("Test: 1");
           var t1 = new StringTests();
           t1.Run();

            Console.WriteLine("\r\nTest: 2");
            // Data retrieval from a XML file
            var t2 = new XmlReadingTest();
           t2.Run();
            Console.WriteLine("\r\nTest:3");

            // Image manipulations
            var t3 = new RescaleImageTest();
            t3.Run();
            Console.WriteLine("\r\nTest: 4");

            // Processing a CSV file
            var t4 = new CsvProcessingTest();
            t4.Run();

            Console.WriteLine("\r\nTest execution ended.");
            Console.ReadKey();
        }
    }
}
