/*
   dinsdag 17 juli 201819:14:24
   User: sa
   Server: DESKTOP-JKG1DO6
   Database: Assets_DB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Assets
	(
	assest_id varchar(100) NOT NULL,
	file_name varchar(100) NOT NULL,
	mime_type varchar(100) NOT NULL,
	created_by varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	country varchar(100) NOT NULL,
	description varchar(1000) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Assets SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.Assets)
	 EXEC('INSERT INTO dbo.Tmp_Assets (assest_id, file_name, mime_type, created_by, email, country, description)
		SELECT assest_id, file_name, mime_type, created_by, email, country, description FROM dbo.Assets WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Assets
GO
EXECUTE sp_rename N'dbo.Tmp_Assets', N'Assets', 'OBJECT' 
GO
ALTER TABLE dbo.Assets ADD CONSTRAINT
	PK_Assets PRIMARY KEY CLUSTERED 
	(
	assest_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.Assets', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Assets', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Assets', 'Object', 'CONTROL') as Contr_Per 