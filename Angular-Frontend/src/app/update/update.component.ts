import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit 
{
  arr ={
  	id: "Hello"
  };

  data_json :{
  	asset_id: string,
  	file_name: string,
  	mime_type: string,
  	created_by: string,
  	email: string,
  	country: string,
  	description: string
  };

	x = "";

	constructor(private route: ActivatedRoute,private http: HttpClient) { }
	base = "http://localhost:62677/DataBase/InfoRow/";
	fullUrl = "";

	ngOnInit() 
	{
		this.x= this.route.snapshot.params['id'];
		this.arr['id']  = this.x.trim();
		this.fullUrl = this.base + this.x.trim();
		console.log(this.fullUrl);

		this.http.get<any>(this.fullUrl).subscribe(data => 
	  	{	
	  		this.data_json = {
				asset_id:data['assest_id'], 
				file_name:data['file_name'], 
				mime_type:data['mime_type'], 
				created_by:data['created_by'], 
				email: data['email'], 
				country:data['country'], 
				description:data['description']
			};
		});
	}
	onUpdate(value: any)
	{
		console.log("rest:", this.arr['id']);
		return	this.http.post<any>("http://localhost:62677/DataBase/UpdateRow/",JSON.stringify(value),{headers: new HttpHeaders().set('Content-Type', 'application/json')})
	    .subscribe(data => { console.log(data)});
	}
}
