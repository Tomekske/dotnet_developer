import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-new-data',
  templateUrl: './new-data.component.html',
  styleUrls: ['./new-data.component.css']
})
export class NewDataComponent implements OnInit {

  constructor(private http: HttpClient) { }
	arr:{
		id: string,
		created_by: string
	}[] = [];
	base = "http://localhost:62677/DataBase/DeleteRow?id=";

 ngOnInit() {
		return this.http.get<any>('http://localhost:62677/DataBase/Dashboard')
		.subscribe(data => {
			this.arr = data;
		})
  }
 

	onImport() 
	{
		alert("Loading data in database, takes ca 3min!");
		return this.http.get('http://localhost:62677/DataBase/ImportData')
		.subscribe(data => {
			if (data['Excecution'] == 'ok')
			{
				alert("Data insterted in database");
			}
		})
	}


	onDelete() 
	{
		return this.http.get('http://localhost:62677/DataBase/DeleteRows')
		.subscribe(data => {
			if (data['Excecution'] == 'ok')
			{
				alert("All records deleted");
			}
		})
	}


	onLoad() 
	{
		return this.http.get<any>('http://localhost:62677/DataBase/Dashboard')
		.subscribe(data => {
			this.arr = data;
		})
	}


	onUpdate() 
	{
		return this.http.get<any>('http://localhost:62677/DataBase/Dashboard')
		.subscribe(data => {
			this.arr = data;
			alert("It may take 20sec to load");
		})
	}


	onDeleteRow(id) 
	{

		return this.http.delete<any>(this.base + id)
		.subscribe(data => {	
			console.log(data);
			location.reload();
		})	
	}
}
