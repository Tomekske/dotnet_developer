﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebExperience.Test.Models;
using WebExperience.Test.Properties;

namespace WebExperience.Test.Controllers
{
    public class DataBaseController : Controller
    {
        public class DB
        {
            public string Excecution { get; set; }
        }

        public class Dash
        {
            public string id { get; set; }
            public string  created_by { get; set; }
        }

        public ActionResult Index()
        {
            return View();
        }

        public string ImportData()
        {
                   
            var csvFile = Resources.AssetImport;
            int counter = 0;
            List<Assets> csv_list = new List<Assets>();
            string[] csvText = csvFile.Split('\n');
            var regex = new Regex("(?<=^|,)(\"(?:[^\"]|\"\")*\"|[^,]*)");

            List<Assets> Asset_List = new List<Assets>();
            int count = 0;

            foreach (string line in csvText.Skip(1))
            {
                List<string> rm_list = new List<string>();
                counter = 0;
                count++;
                System.Diagnostics.Debug.WriteLine("Counter: {0}", count);

                foreach (Match m in regex.Matches(line))
                {
                    counter++;    
                    rm_list.Add(m.Value);
                }

                    if (counter == 7)
                    {
                        Assets_DBEntities db = new Assets_DBEntities();
                        Assets csv = new Assets { assest_id = rm_list[0], file_name = rm_list[1], mime_type = rm_list[2], created_by = rm_list[3], email = rm_list[4], country = rm_list[5], description = rm_list[6] };
                        Asset_List.Add(csv);
                        db.Assets.Add(csv);
                        db.SaveChanges();
                    }
            }
            
            DB db_json = new DB { Excecution = "ok" };
            return JsonConvert.SerializeObject(db_json);
        }

        public string DeleteRows()
        {
            Assets_DBEntities db = new Assets_DBEntities();
            db.Database.ExecuteSqlCommand("TRUNCATE TABLE [Assets_DB].[dbo].[Assets]");
            DB db_json = new DB { Excecution = "ok" };
            return JsonConvert.SerializeObject(db_json);
        }
        public string Dashboard()
        {
            Assets_DBEntities db = new Assets_DBEntities();
            List<Assets> List_Rows = db.Assets.ToList();
            List<Dash> List_Dashboard = new List<Dash>();

            foreach(Assets assets in List_Rows)
            {
                List_Dashboard.Add(new Dash { id = assets.assest_id, created_by = assets.created_by});
            }

            System.Diagnostics.Debug.WriteLine(List_Rows[0].country);
            DB db_json = new DB { Excecution = "ok" };
            return JsonConvert.SerializeObject(List_Dashboard);
        }

        public string DeleteRow(string id)
        {
            string x = string.Format("DELETE FROM [Assets_DB].[dbo].[Assets] WHERE assest_id='{0}';", id);
            Assets_DBEntities db = new Assets_DBEntities();
            db.Database.ExecuteSqlCommand("DELETE FROM [Assets_DB].[dbo].[Assets] WHERE assest_id={0}", id);
            System.Diagnostics.Debug.WriteLine(x);
            db.SaveChanges();
            DB db_json = new DB { Excecution = id };
            return JsonConvert.SerializeObject(db_json);
        }

        public string InfoRow(string id)
        {
            Assets_DBEntities db = new Assets_DBEntities();
            Assets assets = db.Assets.FirstOrDefault(d => d.assest_id == id);
            return JsonConvert.SerializeObject(assets);
        }

        [HttpPost]
        public string UpdateRow(string id)
        {
            //Pseudo code starts here, because got an ERROR 404 because of the [HttpPost]
            /*
            System.Diagnostics.Debug.WriteLine("heeey: " + id);
            //Response.Redirect
            Assets_DBEntities db = new Assets_DBEntities();
            Assets assets = db.Assets.FirstOrDefault(d => d.assest_id == id);
            assets.assest_id = id;
            assets.mime_type = mime_type;
            assets.file_name = file_name;
            assets.country = country;
            assets.email = email;
            assets.created_by = created_by;
            assets.description = description;
            db.saveChanges();
            */
            DB db_json = new DB { Excecution = "ok" };
            return JsonConvert.SerializeObject(db_json);
        }
    }
}