﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebExperience.Test.Models
{
    public class AssetsModel
    {
        public string assest_id { get; set; }
        public string file_name { get; set; }
        public string mime_type { get; set; }
        public string created_by { get; set; }
        public string email { get; set; }
        public string country { get; set; }
        public string description { get; set; }
    }
}